<?php


namespace App\Validations;


use App\Helpers\ResponseHelper;
use Respect\Validation\Validator as v;

class MensajeValidation
{
    public static function validate(array $model)
    {
        try {
            $v = v::key('mensaje', v::stringType()->notEmpty())
                ->key('cmbMensaje', v::numeric()->notEmpty());

            $v->assert($model);
        } catch (\Exception $e) {
            $rh = new ResponseHelper();
            $rh->setResponse(false, null);
            $rh->validations = $e->findMessages([
                'mensaje' => 'Campo es requerido',
                'cmbMensaje' => 'Campo es requerido',

            ]);

            exit(json_encode($rh));
        }
    }


    public static function validateRegistro(array $model)
    {
        try {
            $v = v::key('celular', v::stringType()->min(10)->notEmpty())
                ->key('edad', v::numeric()->min(16)->notEmpty())
                ->key('sexo', v::stringType()->notEmpty())
                ->key('chkacepto', v::stringType()->notEmpty());



            $v->assert($model);
        } catch (\Exception $e) {
            $rh = new ResponseHelper();
            $rh->setResponse(false, null);
            $rh->validations = $e->findMessages([
                'celular' => 'Campo es requerido o debe tener min. 10 dig.',
                'edad' => 'Campo es requerido o debe ser mayor de 16 años',
                'sexo' => 'Campo es requerido',
                'chkacepto' => 'Debe aceptar los terminos y condiciones',

            ]);

            exit(json_encode($rh));
        }
    }

    public static function validateRegistroCodigo(array $model)
    {
        try {
            $v = v::key('celular', v::stringType()->min(10)->notEmpty())
                ->key('edad', v::numeric()->min(16)->notEmpty())
                ->key('sexo', v::stringType()->notEmpty())
                ->key('chkacepto', v::stringType()->notEmpty())
                    ->key('codigo', v::stringType()->notEmpty());



            $v->assert($model);
        } catch (\Exception $e) {
            $rh = new ResponseHelper();
            $rh->setResponse(false, null);
            $rh->validations = $e->findMessages([
                'celular' => 'Campo es requerido o debe tener min. 10 dig.',
                'edad' => 'Campo es requerido o debe ser mayor de 16 años',
                'sexo' => 'Campo es requerido',
                'chkacepto' => 'Debe aceptar los terminos y condiciones',
                'codigo' => 'Codigo no valido, puede tardar hasta 3 a 4 min',

            ]);

            exit(json_encode($rh));
        }
    }

}