<?php


namespace App\Validations;


use App\Helpers\ResponseHelper;
use Respect\Validation\Validator as v;

class NumeroValidation
{

    public static function validate (array $model) {
        try{
            $v = v::key('prioridad', v::stringType()->notEmpty())
                ->key('numeros', v::stringType()->notEmpty());

            $v->assert($model);
        } catch (\Exception $e) {
            $rh = new ResponseHelper();
            $rh->setResponse(false, null);
            $rh->validations = $e->findMessages([
                'prioridad' => 'Campo es requerido',
                'numeros' => 'Campo es requerido',

            ]);

            exit(json_encode($rh));
        }
    }
}