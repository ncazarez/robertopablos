<?php

namespace App\Controllers;

use App\Helpers\ResponseHelper;
use App\Helpers\UrlHelper;
use App\Models\Celular;
use App\Repositories\CelularRepository;
use App\Repositories\LandingRepository;
use App\Validations\MensajeValidation;
use Core\{Auth, Controller, Log};
use Core\ServicesContainer;
use Twilio\Rest\Client;

class HomeController extends Controller
{
    private $config;

    public function __construct()
    {
        parent::__construct();
        $this->config = ServicesContainer::getConfig();
    }

    public function getIndex()
    {
        /*return $this->render('home/index.twig', [
            'title' => 'Inicio'
        ]);*/
        UrlHelper::redirect('home/espuma');


    }

    public function getespuma()
    {
        return $this->render('home/espuma.twig', []);
    }

    public function postobtenercodigo()
    {
        MensajeValidation::validateRegistro($_POST);
        $rh = new ResponseHelper();
        $codigo = substr(md5(uniqid(mt_rand(), true)), 0, 5);
        $model = new Celular();
        $model->numero = $_POST['celular'];
        $model->prioridad = 3;
        $model->edad = $_POST['edad'];
        $model->acepto = 1;
        $model->codigo = $codigo;
        $model->confirmado = 0;
        $model->empresa_id=$_POST['empresa'];
        $rh = (new CelularRepository())->guardar($model);
        if ($rh->response) {
            $twilio = $this->config['twilio'];
            try {
                $cliente = new Client($twilio['account_sid'], $twilio['auth_token']);
                $cliente->messages->create('+52' . $model->numero, array(
                    'from' => $twilio['number'],
                    'body' => 'TU CODIGO ES: '.$model->codigo
                ));
            } catch (\Exception $e) {
                Log::warning(AdministrarController::class, $e->getMessage() . '   ->' . json_encode($celular));
            }

            $rh->message = 'Se envio un sms con el código';
        }

        print_r(json_encode($rh));

    }

    public function postvalidarcodigo()
    {
        MensajeValidation::validateRegistroCodigo($_POST);
        $model = (new CelularRepository())->obtenerByPhoneYCode($_POST['celular'], $_POST['codigo']);
        $rh = new ResponseHelper();
        if (is_object($model)) {
            $model->confirmado = 1;
            $rh = (new CelularRepository())->guardar($model);
        }

        if ($rh->response) {
            $rh->href = 'home/espuma';
        }
        print_r(json_encode($rh));

    }


    public function getSignin()
    {
        Auth::signIn([
            'id' => 1,
            'name' => 'Eduardo',
        ]);
    }

    public function getUser()
    {
        var_dump(Auth::getCurrentUser());
    }

    public function getIsloggedin()
    {
        return var_dump(Auth::isLoggedIn());
    }


}