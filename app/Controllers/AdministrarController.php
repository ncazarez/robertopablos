<?php

namespace App\Controllers;


use App\Helpers\ResponseHelper;
use App\Helpers\UrlHelper;
use App\Models\Celular;
use App\Repositories\CelularesRepository;
use App\Repositories\CelularRepository;
use App\Repositories\MensajesRepository;
use App\Validations\MensajeValidation;
use App\Validations\NumeroValidation;
use Core\Auth;
use Core\Controller;
use Core\Log;
use Core\ServicesContainer;
use Twilio\Rest\Client;

class AdministrarController extends Controller
{
    private $config;
    public function __construct()
    {
        if (!Auth::isLoggedIn()) {
            UrlHelper::redirect();
        }
        parent::__construct();
        $this->config=ServicesContainer::getConfig();
    }

    public function getindex()
    {
        return $this->render('administrar/index.twig', [
            'title' => 'Administrar',
            'menu' => true
        ]);
    }


    public function getsendsms()
    {
        return $this->render('administrar/sendsms.twig', [
            'title' => 'Envio de SMS',
            'mensajes' => (new MensajesRepository())->listar()
        ]);
    }

    public function postenviarsms()
    {
        MensajeValidation::validate($_POST);

        $rh = new ResponseHelper();
        $mensajeId = $_POST['cmbMensaje'];
        $texto = $_POST['mensaje'];
        $telefonos = [];
        $twilio = $this->config['twilio'];
        try {
            $celulares = (new CelularRepository())->listar();
            foreach ($celulares as $celular) {
                $resp=(new MensajesRepository())->seenviomensaje($mensajeId,$celular->id);

                if(!$resp) {
                    if ($this->config['environment'] == 'prod') {
                        try{
                        $cliente = new Client($twilio['account_sid'], $twilio['auth_token']);
                        $cliente->messages->create('+52' . $celular->numero, array(
                            'from' => $twilio['number'],
                            'body' => $texto
                        ));
                        }catch (\Exception $e){
                            Log::warning(AdministrarController::class, $e->getMessage() . '   ->'.json_encode( $celular));
                        }
                    }
                    $telefonos[] = $celular->numero;
                    (new MensajesRepository())->registrarEnvio($mensajeId, $celular->id);
                }
            }
            $rh->setResponse(true, 'Se enviaron ' . count($telefonos) . ' mensajes');
        } catch (\Exception $e) {
            Log::error(AdministrarController::class, $e->getMessage() . "  Enviados a: " . $telefonos);
        }
        print_r(json_encode($rh));
    }

    public function postobtenermensajebyid()
    {
        $model = (new MensajesRepository())->obtener($_POST['id']);
        $rh = new ResponseHelper();
        if (!is_object($model)) {
            $rh->message = 'Mensaje no existe';
        } else {
            $rh->setResponse(true);
            $rh->result = $model->texto;
        }
        print_r(json_encode($rh));
    }



    public function getnuevonumero(){
        return $this->render('administrar/nnumero.twig', [
            'title' => 'Envio de SMS',
            'mensajes' => (new MensajesRepository())->listar()
        ]);
    }

    public function postguadarnumeros(){
        NumeroValidation::validate($_POST);
        $prioridad=$_POST['prioridad'];
        $numeros=explode(',',$_POST['numeros']);
        $contador=0;
        foreach ($numeros as $numero){
            //if(count( $numero)==10) {
                $model = new Celular();
                $model->numero = trim($numero);
                $model->prioridad = $prioridad;
                $rh = (new CelularRepository())->guardar($model);
                $contador++;
            //}
        }
        print_r(json_encode($rh));
    }


    /*    public function postsendsms()
        {
            $rh = new ResponseHelper();
            $twilio = $this->config['twilio'];
            try {
                $cliente = new Client($twilio['account_sid'], $twilio['auth_token']);
                try {
                    $cliente->messages->create(
                        '+52' . $_POST['txttelefono'],
                        array(
                            'from' => $twilio['number'],
                            'body' => $_POST['txtmensaje']
                        )
                    );
                } catch (\Exception $e) {
                }
                $rh->setResponse(true);
                $rh->result = "Enviado";
            } catch (\Exception $e) {
                Log::error(CpanelController::class, $e->getMessage());
            }
            print_r(json_encode($rh));
        }*/
    /*
        public function postsendsmsall()
        {
            $rh = new ResponseHelper();
            $twilio = $this->config['twilio'];
            try {
                $datos = $this->cliente->listar();
                $contador = 0;
                foreach ($datos as $item) {
                    if ($item->esborrado == 0) {
                        $cliente = new Client($twilio['account_sid'], $twilio['auth_token']);
                        try {
                            $cliente->messages->create(
                                '+52' . $item->celular,
                                array(
                                    'from' => $twilio['number'],
                                    'body' => $_POST['txtmensaje']
                                )
                            );
                            $contador++;
                        } catch (\Exception $e) {

                        }
                    }
                }
                $rh->setResponse(true);
                $rh->result = $contador;
            } catch (\Exception $e) {
                Log::error(CpanelController::class, $e->getMessage());
            }
            print_r(json_encode($rh));
        }
        */

    /*
        public function getsmscelulares(){
            // $msg="SEXY HALLOWEEN - SAB 27 OCT - 10pm a 3am PARA 18+ Emiratos Night Club Cover. Muj $50, Hom $100 REGGAETON NIGHT";
            //$msg="Hoy es SEXY HALLOWEEN / SABADO 27 OCTUBRE MAYORES DE EDAD. Emiratos Night Club  / 10pm. a 3am Cover. Mujeres$50, Hombres$100 REGGAETON NIGHT";
            $msg='PREMIOS..CHICAS MAS SEXIS, BOTELLAS, CUBETAS & SHOTS PROMOS: Red o Tradicional$550/Nacionales$200/Cubeta$250 valido a 10:30pm. Aplican restricciones';
            $rh = new ResponseHelper();
            $twilio = $this->config['twilio'];

            try {
                $datos = (new CelularesRepository())->listar();
                $contador = 0;


    //http://registroshowroom.com/panel/smscelulares

                foreach ($datos as $item) {
                    $cliente = new Client($twilio['account_sid'], $twilio['auth_token']);
                    try {
                        $cliente->messages->create(
                            '+52' . $item->telefono,
                            array(
                                'from' => $twilio['number'],
                                'body' => $msg
                            )
                        );
                        $contador++;
                    } catch (\Exception $e) {

                    }
                }
                $rh->setResponse(true);
                $rh->result = $contador;
            } catch (\Exception $e) {
                Log::error(CpanelController::class, $e->getMessage());
            }
            print_r(json_encode($rh));
        }*/


}