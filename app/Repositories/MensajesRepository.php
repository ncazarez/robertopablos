<?php


namespace App\Repositories;


use App\Models\Mensaje;
use App\Models\MensajeEnviado;
use Core\Log;
use Illuminate\Database\Eloquent\Collection;

class MensajesRepository
{
    private $model;

    public function __construct()
    {
        $this->model = new Mensaje();
    }

    public function listar(): ?Collection
    {
        $data = [];
        try {
            $data = $this->model->orderBy('nombre')->get();
        } catch (\Exception $e) {
            Log::error(CelularRepository::class, $e->getMessage() . " Linea: " . $e->getLine());
        }
        return $data;
    }

    public function obtener($id): ?Mensaje
    {
        $dato = null;
        try {
            $dato = $this->model->find($id);
        } catch (\Exception $e) {
            Log::error(MensajesRepository::class, $e->getMessage() . " Linea: " . $e->getLine());
        }
        return $dato;
    }

    public function registrarEnvio($mensajeId, $celularId)
    {
        $model = new MensajeEnviado();
        try {
            $model->mensaje_id = $mensajeId;
            $model->celular_id = $celularId;
            $model->save();
        } catch (\Exception $e) {
            Log::error(MensajesRepository::class, $e->getMessage());
        }
    }

    public function seenviomensaje($mensajeId, $celularId): bool
    {
        $resp = false;
        try {
            $mEnviado=new MensajeEnviado();
            $obj=$mEnviado->where('mensaje_id',$mensajeId)
                ->where('celular_id',$celularId)
                ->first();
            if(is_object($obj)){
                $resp=true;
            }
        } catch (\Exception $e) {
            Log::error(MensajesRepository::class, $e->getMessage() . " Linea: " . $e->getLine());
        }
        return $resp;
    }
}