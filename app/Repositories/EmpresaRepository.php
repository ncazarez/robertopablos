<?php


namespace App\Repositories;


use App\Helpers\ResponseHelper;
use App\Models\Empresa;
use Core\Log;
use Illuminate\Database\Eloquent\Collection;

class EmpresaRepository
{
    private $model;

    public function __construct()
    {
        $this->model = new Empresa();
    }

    public function listar(): Collection
    {
        $datos = [];
        try {
            $datos = $this->model->orderBy('nombre')->get();
        } catch (\Exception $e) {
            Log::error(EmpresaRepository::class, $e->getMessage() . " Linea: " . $e->getLine());
        }
        return $datos;
    }

    public function obtener($id): ?Empresa
    {
        $datos = null;
        try {
            $datos = $this->model->find($id);
        } catch (\Exception $e) {
            Log::error(EmpresaRepository::class, $e->getMessage() . " Linea: " . $e->getLine());
        }
        return $datos;
    }

    public function guardar($model): ResponseHelper
    {
        $rh = new ResponseHelper();
        try {
            $this->model=$model;
            if(isset($model->id)){
                $this->model->exists=true;
            }
            $this->model->save();
            $rh->setResponse(true,'Registro exitoso');
        } catch (\Exception $e) {
            Log::error(EmpresaRepository::class, $e->getMessage() . " Linea: " . $e->getLine());
        }
        return $rh;
    }

}