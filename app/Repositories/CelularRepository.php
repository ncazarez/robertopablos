<?php


namespace App\Repositories;


use App\Helpers\ResponseHelper;
use App\Models\Celular;
use Core\Log;
use Illuminate\Database\Eloquent\Collection;

class CelularRepository
{
    private $model;

    public function __construct()
    {
        $this->model = new Celular();
    }

    public function listar(): ?Collection
    {
        $data = [];
        try {
            $data = $this->model->orderBy('prioridad')->get();
        } catch (\Exception $e) {
            Log::error(CelularRepository::class, $e->getMessage() . " Linea: " . $e->getLine());
        }
        return $data;
    }

    public function guardar($model): ResponseHelper
    {
        $rh = new ResponseHelper();
        try {
            $this->model = $model;
            if (!empty($model->id)) {
                $this->model->exists = true;
            }
            $this->model->save();
            $rh->setResponse(true);
        } catch (\Exception $e) {
            Log::error(CelularRepository::class, $e->getMessage() . " Linea: " . $e->getLine());
        }
        return $rh;
    }

    public function obtenerByPhoneYCode($phone, $code): ?Celular
    {
        $data = null;
        try {
            $data=$this->model->where('numero',$phone)
                ->where('codigo',$code)
                ->first();
        } catch (\Exception $e) {
            Log::error(CelularRepository::class, $e->getMessage() . " Linea: " . $e->getLine());
        }
        return $data;
    }
}