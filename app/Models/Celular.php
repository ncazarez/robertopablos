<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Celular extends Model
{

    protected $table='celulares';

    public function mensajeenviado(){
        return $this->hasMany('App\Models\MensajeEnviado');
    }

    public function empresa(){
        return $this->belongsTo('App\Models\Empresa');
    }
}