<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class MensajeEnviado extends Model
{
    protected $table='mensajes_enviados';

    public function mensaje(){
        return $this->belongsTo('App\Models\Mensaje');
    }

}