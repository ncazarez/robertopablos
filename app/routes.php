<?php
/* Controllers */
$router->group(['before' => 'auth'], function($router){

    $router->controller('/cpanel', 'App\\Controllers\\AdministrarController');
});

$router->controller('/home', 'App\\Controllers\\HomeController');

$router->controller('/auth', 'App\\Controllers\\AuthController');


$router->get('/', function(){
    if(!\Core\Auth::isLoggedIn()){
        \App\Helpers\UrlHelper::redirect('home');
    } else {
        \App\Helpers\UrlHelper::redirect('cpanel');
    }

});

$router->get('/welcome', function(){
    return 'Welcome page';
}, ['before' => 'auth']);

$router->get('/test', function(){
    return 'Welcome page';
}, ['before' => 'auth']);